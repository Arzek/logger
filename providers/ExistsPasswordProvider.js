const { ServiceProvider } = require('@adonisjs/fold')

class ExistsRuleProvider extends ServiceProvider {
  async existsFn(data, field, message, args, get) {
    const value = get(data, field)
    if (!value) {
      /**
       * skip validation if value is not defined. `required` rule
       * should take care of it.
       */
      return
    }

    const row = await use('Database').table('users').where('email', data.email).first()
    if (!row.password) {
      throw message
    }
  }

  boot() {
    use('Validator').extend('exists-password-by-email', this.existsFn.bind(this))
  }
}

module.exports = ExistsRuleProvider