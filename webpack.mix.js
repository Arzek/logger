const mix = require('laravel-mix');

mix.react('./resources/js/app.js', './public/')
  .sass('./resources/scss/app.scss', './public/')
  .setPublicPath('./public')
  .sourceMaps();