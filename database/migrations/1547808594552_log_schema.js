'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogSchema extends Schema {
  up() {
    this.create('logs', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('cascade')
      table.integer('project_id').unsigned().references('id').inTable('projects').onDelete('cascade')
      table.string('env', 80).notNullable()
      table.enu('level', [
        'DEBUG',
        'INFO',
        'NOTICE',
        'WARNING',
        'ERROR',
        'CRITICAL',
        'ALERT',
        'EMERGENCY'
      ]).defaultTo('INFO').notNullable()
      table.string('message', 255).notNullable()
      table.json('context').notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('logs')
  }
}

module.exports = LogSchema
