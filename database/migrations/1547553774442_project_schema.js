'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectSchema extends Schema {
  up() {
    this.create('projects', (table) => {
      table.increments()
      table.string('name', 80).notNullable()
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('cascade')
      table.string('token', 255).notNullable().unique()
      table.timestamps()
    })
  }

  down() {
    this.drop('projects')
  }
}

module.exports = ProjectSchema
