'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up() {
    this.create('users', table => {
      table.increments()
      table.string('name').nullable()
      table.string('avatar').nullable()
      table.string('username', 80).notNullable()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).nullable()
      table.string('provider_id').nullable().unique()
      table.string('provider').nullable()
      table.integer('limit_logs', 100).notNullable()
      table.integer('limit_projects', 100).notNullable()
      table.integer('limit_requests', 100).notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('users')
  }
}

module.exports = UserSchema
