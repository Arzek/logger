'use strict'

const faker = require('faker');

const { test, trait } = use('Test/Suite')('Log')

const LogRepository = use('App/Repositories/LogRepository')
const UserRepository = use('App/Repositories/UserRepository')
const ProjectRepository = use('App/Repositories/ProjectRepository')

trait('Auth/Client')
trait('Session/Client')
trait('Test/ApiClient')

async function createUser() {
  return UserRepository.create({
    username: faker.name.findName(),
    email: faker.internet.email(),
    password: faker.internet.password()
  })
}

async function createProject(userId) {
  return ProjectRepository.create({
    name: faker.name.findName(),
    userId
  })
}

async function createLog(projectId, userId) {
  return LogRepository.create({
    userId,
    projectId,
    env: 'localhost',
    level: 'INFO',
    message: 'Test message',
    context: { data: { id: 7 } }
  })
}


test('Create log', async ({ client, assert }) => {
  const user = await createUser()
  const project = await createProject(user.id)

  const response = await client.post(`/api/logs`)
    .header('accept', 'application/json')
    .send({
      token: project.token,
      env: 'localhost',
      level: 'INFO',
      message: 'Test message',
      context: {
        data: { id: 7 }
      }
    })
    .end()

  response.assertStatus(200)

  assert.deepEqual(
    {
      level: response.body.level,
      message: response.body.message
    },
    {
      level: 'INFO',
      message: 'Test message'
    }
  )

  const log = await LogRepository.find(response.body.id)
  await LogRepository.deleteById(log.id)
  await ProjectRepository.deleteById(project.id)
  await UserRepository.deleteById(user.id)
})

test('Create log no token', async ({ client }) => {
  const user = await createUser()
  const project = await createProject(user.id)

  const response = await client.post(`/api/logs`)
    .header('accept', 'application/json')
    .send({
      env: 'localhost',
      level: 'INFO',
      message: 'Test message',
      context: {
        data: { id: 7 }
      }
    })
    .end()

  response.assertStatus(400)

  await ProjectRepository.deleteById(project.id)
  await UserRepository.deleteById(user.id)
})

test('Get all logs', async ({ client, assert }) => {
  const user = await createUser()
  const project = await createProject(user.id)
  const log = await createLog(project.id, user.id)

  const response = await client.get(`/api/logs/${project.id}`)
    .loginVia(user)
    .end()

  response.assertStatus(200)

  assert.deepEqual(
    {
      id: response.body[0].id,
      env: response.body[0].env,
      level: response.body[0].level
    },
    {
      id: log.id,
      env: log.env,
      level: log.level
    }
  )

  await LogRepository.deleteById(log.id)
  await ProjectRepository.deleteById(project.id)
  await UserRepository.deleteById(user.id)
})

test('Get log by id', async ({ client, assert }) => {
  const user = await createUser()
  const project = await createProject(user.id)
  const log = await createLog(project.id, user.id)

  const response = await client.get(`/api/logs/${project.id}/${log.id}`)
    .loginVia(user)
    .end()

  response.assertStatus(200)

  assert.deepEqual(
    {
      id: response.body.id,
      env: response.body.env,
      level: response.body.level
    },
    {
      id: log.id,
      env: log.env,
      level: log.level
    }
  )

  await LogRepository.deleteById(log.id)
  await ProjectRepository.deleteById(project.id)
  await UserRepository.deleteById(user.id)
})

test('Delete log', async ({ client }) => {
  const user = await createUser()
  const project = await createProject(user.id)
  const log = await createLog(project.id, user.id)

  const response = await client.delete(`/api/logs/${project.id}/${log.id}`)
    .loginVia(user)
    .end()

  response.assertStatus(200)

  await ProjectRepository.deleteById(project.id)
  await UserRepository.deleteById(user.id)
})

