'use strict'
const faker = require('faker');

const { test, trait } = use('Test/Suite')('Project')

const UserRepository = use('App/Repositories/UserRepository')
const ProjectRepository = use('App/Repositories/ProjectRepository')

trait('Auth/Client')
trait('Session/Client')
trait('Test/ApiClient')


async function createUser() {
	return UserRepository.create({
		username: faker.name.findName(),
		email: faker.internet.email(),
		password: faker.internet.password()
	})
}

async function createProject(userId) {
	return ProjectRepository.create({
		name: faker.name.findName(),
		userId
	})
}

test('Get all projects', async ({ client, assert }) => {

	const user = await createUser()
	const project = await createProject(user.id)

	const response = await client.get(`/api/projects`)
		.loginVia(user)
		.end()

	response.assertStatus(200)

	assert.deepEqual(
		{
			id: response.body[0].id,
			name: response.body[0].name,
			token: response.body[0].token
		},
		{
			id: project.id,
			name: project.name,
			token: project.token
		}
	)

	await ProjectRepository.deleteById(project.id)
	await UserRepository.deleteById(user.id)
});

test('Get one projects', async ({ client, assert }) => {

	const user = await createUser()
	const project = await createProject(user.id)

	const response = await client.get(`/api/project/${project.id}`)
		.loginVia(user)
		.end()

	response.assertStatus(200)

	assert.deepEqual(
		{
			id: response.body.id,
			name: response.body.name,
			token: response.body.token
		},
		{
			id: project.id,
			name: project.name,
			token: project.token
		}
	)

	await ProjectRepository.deleteById(project.id)
	await UserRepository.deleteById(user.id)
});



test('Create project', async ({ client, assert }) => {

	const user = await createUser()

	const name = faker.name.findName();
	const response = await client.post('/api/projects')
		.loginVia(user)
		.header('accept', 'application/json')
		.send({
			name
		})
		.end()

	response.assertStatus(200)


	const project = await ProjectRepository.findByNameAndUserId({
		userId: user.id,
		name
	})

	assert.deepEqual(
		{
			id: response.body.id,
			name: response.body.name,
			token: response.body.token
		},
		{
			id: project.id,
			name: project.name,
			token: project.token
		}
	)

	await ProjectRepository.deleteById(project.id)
	await UserRepository.deleteById(user.id)
})

test('Create project ( overflow limit )', async ({ client, assert }) => {

	const user = await createUser()

	const name = faker.name.findName();

	let response = await client.post('/api/projects')
		.loginVia(user)
		.header('accept', 'application/json')
		.send({
			name
		})
		.end()

	response.assertStatus(200)


	const project = await ProjectRepository.findByNameAndUserId({
		userId: user.id,
		name
	})

	assert.deepEqual(
		{
			id: response.body.id,
			name: response.body.name,
			token: response.body.token
		},
		{
			id: project.id,
			name: project.name,
			token: project.token
		}
	)

	const newName = 'New' + name
	response = await client.post('/api/projects')
		.loginVia(user)
		.header('accept', 'application/json')
		.send({
			name: newName
		})
		.end()

	response.assertStatus(401)

	await ProjectRepository.deleteById(project.id)
	await UserRepository.deleteById(user.id)
})

test('Create project (no auth)', async ({ client }) => {
	const name = 'Project name';
	const response = await client.post('/api/projects')
		.header('accept', 'application/json')
		.send({
			name
		})
		.end()

	response.assertStatus(401)
})

test('Edit project', async ({ client, assert }) => {

	const user = await createUser()
	const project = await createProject(user.id)

	const newName = faker.name.findName()
	const response = await client.put(`/api/projects/${project.id}`)
		.loginVia(user)
		.header('accept', 'application/json')
		.send({
			name: newName
		})
		.end()

	response.assertStatus(200)

	assert.deepEqual(
		{
			name: response.body.name
		},
		{
			name: newName
		}
	)

	await ProjectRepository.deleteById(project.id)
	await UserRepository.deleteById(user.id)
})

test('Edit project (no auth)', async ({ client }) => {
	const user = await createUser()
	const project = await createProject(user.id)

	const newName = faker.name.findName()
	const response = await client.put(`/api/projects/${project.id}`)
		.header('accept', 'application/json')
		.send({
			name: newName
		})
		.end()

	response.assertStatus(401)

	await ProjectRepository.deleteById(project.id)
	await UserRepository.deleteById(user.id)
})

test('Delete project', async ({ client, assert }) => {
	const user = await createUser()
	let project = await createProject(user.id)

	const response = await client.delete(`/api/projects/${project.id}`)
		.loginVia(user)
		.header('accept', 'application/json')
		.end()

	response.assertStatus(200)
	response.assertJSONSubset({
		status: true
	})

	project = await ProjectRepository.find(project.id)
	assert.equal(null, project)

	await UserRepository.deleteById(user.id)
})

test('Delete project (no auth)', async ({ client }) => {
	const user = await createUser()
	const project = await createProject(user.id)

	const response = await client.delete(`/api/projects/${project.id}`)
		.header('accept', 'application/json')
		.end()

	response.assertStatus(401)

	await ProjectRepository.deleteById(project.id)
	await UserRepository.deleteById(user.id)
})

