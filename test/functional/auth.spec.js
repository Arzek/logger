'use strict'

const faker = require('faker');

const { test, trait } = use('Test/Suite')('Auth')
const UserRepository = use('App/Repositories/UserRepository')

trait('Test/ApiClient')

test('Login user success', async ({ client, assert }) => {
  const reqData = {
    email: faker.internet.email(),
    password: faker.internet.password()
  }

  await UserRepository.create({
    username: faker.name.findName(),
    email: reqData.email,
    password: reqData.password
  })

  const response = await client.post('/api/login')
    .header('accept', 'application/json')
    .send(reqData)
    .end()

  response.assertStatus(200)

  await UserRepository.deleteByEmail(reqData.email)
})

test('Login user error', async ({ client }) => {
  const response = await client.post('/api/login')
    .header('accept', 'application/json')
    .send({
      email: faker.internet.email(),
      password: faker.internet.password()
    })
    .end()

  response.assertStatus(400)
})

test('Login user error (no params)', async ({ client }) => {
  const response = await client.post('/api/login')
    .header('accept', 'application/json')
    .end()

  response.assertStatus(400)
})


