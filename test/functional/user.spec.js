'use strict'

const faker = require('faker');

const { test, trait } = use('Test/Suite')('User')
const UserRepository = use('App/Repositories/UserRepository')


trait('Test/ApiClient')

test('Create user request', async ({ client, assert }) => {

  const password = faker.internet.password()
  const reqData = {
    username: faker.name.findName(),
    email: faker.internet.email(),
    password: password,
    passwordRepeat: password
  }

  const response = await client.post('/api/user')
    .header('accept', 'application/json')
    .send(reqData)
    .end()

  response.assertStatus(200)
 
  await UserRepository.deleteByEmail(reqData.email)
})

test('Create user error request', async ({ client }) => {
  const response = await client.post('/api/user')
    .header('accept', 'application/json')
    .end()

  response.assertStatus(400)
})
