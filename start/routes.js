'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */

const Route = use('Route')


// Route.get('/', 'AuthController.test')

/** Auth routes */
Route.get('/auth/:provider', 'AuthController.redirectToProvider')
  .as('social.login')

Route.get('/authenticated/:provider', 'AuthController.handleProviderCallback')
  .as('social.login.callback')

/** Api routes */

Route.post('/api/user', 'UserController.create')
  .validator('User')

Route.get('/api/user', 'UserController.get')
  .middleware('auth')

Route.post('/api/login', 'AuthController.login')
  .validator('Login')

Route.get('/api/projects', 'ProjectController.getAll')
  .middleware('auth')

Route.post('/api/projects', 'ProjectController.create')
  .middleware('auth')
  .validator('Project')

Route.get('/api/project/:id', 'ProjectController.getById')
  .middleware('auth')

Route.put('/api/projects/:id', 'ProjectController.edit')
  .middleware('auth')
  .validator('Project')

Route.delete('/api/projects/:id', 'ProjectController.delete')
  .middleware('auth')

Route.post('/api/logs', 'LogController.create')
  .validator('Log')

Route.get('/api/logs/:projectId', 'LogController.getAll')
  .middleware('auth')

Route.get('/api/logs/:projectId/:id', 'LogController.getOne')
  .middleware('auth')

Route.delete('/api/logs/:projectId/:id', 'LogController.delete')
  .middleware('auth')


