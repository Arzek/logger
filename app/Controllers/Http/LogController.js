'use strict'

const WS = use('App/Helpers/WS')
const Env = use('Env')
const Throttle = use('Throttle')
const LogRepository = use('App/Repositories/LogRepository')
const UserRepository = use('App/Repositories/UserRepository')
const ProjectRepository = use('App/Repositories/ProjectRepository')

class LogController {
  async create({ request, response }) {
    const {
      token,
      env,
      level,
      message,
      context = {} } = request.all()

    const project = await ProjectRepository.findByToken(token)
    const user = await UserRepository.find(project.user_id);

    Throttle.resource(user.id, user.getLimitRequests(), Env.get('DECAY_IN_SECONDS'))
    if (!Throttle.attempt()) {
      return response.status(429).json({
        error: 'Too Many Requests'
      })
    }

    const countLogs = +(await LogRepository.getCountLogs({ userId: user.id, projectId: project.id }))

    if (countLogs >= user.getLimitLogs()) {
      await LogRepository.deleteLast({ userId: user.id, projectId: project.id })
    }

    const log = await LogRepository.create({
      userId: user.id,
      projectId: project.id,
      env,
      level,
      message,
      context
    })

    response.send(log);

    WS.emit('event', { token, log, projectId: project.id, countLogs: await project.logs().getCount() })
  }

  async getAll({ auth, params }) {
    const user = await auth.getUser()
    const { projectId } = params
    return await LogRepository.getAll({ projectId, userId: user.id })
  }

  async getOne({ auth, params }) {
    const user = await auth.getUser()
    const { id, projectId } = params
    return await LogRepository.getOne({ id, projectId, userId: user.id })
  }

  async delete({ auth, params, response }) {
    const user = await auth.getUser()
    const { id, projectId } = params

    await LogRepository.deleteOneByIds({ id, projectId, userId: user.id })

    response.send({
      status: true
    })
  }
}

module.exports = LogController
