'use strict'

const Env = use('Env')
const UserRepository = use('App/Repositories/UserRepository')

class AuthController {
  async login({ request, auth }) {
    const { email, password } = request.all()
    const token = await auth.attempt(email, password)
    return {
      token
    }
  }

  async redirectToProvider({ ally, params }) {
    await ally.driver(params.provider).redirect()
  }

  async handleProviderCallback({ params, ally, auth, response, request }) {
    const { provider } = params
    try {
      const userData = await ally.driver(params.provider).getUser()
      const authUser = await UserRepository.findByEmail(userData.getEmail())

      if (authUser) {
        await UserRepository.deleteTokensByUserId(authUser.id)
        const token = await auth.generate(authUser)
        return response.redirect(`${Env.get('FE_AUTH')}?token=${token.token}`)
      }

      const user = await UserRepository.create({
        name: userData.getName(),
        username: userData.getNickname(),
        email: userData.getEmail(),
        avatar: userData.getAvatar(),
        provider: provider,
        providerId: userData.getId()
      })

      await UserRepository.deleteTokensByUserId(user.id)
      const token = await auth.generate(user)
      return response.redirect(`${Env.get('FE_AUTH')}?token=${token.token}`)
    } catch (e) {
      //response.redirect('/auth/' + provider)
      console.log(e);
    }
  }
}

module.exports = AuthController
