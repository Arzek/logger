'use strict'

const ProjectRepository = use('App/Repositories/ProjectRepository')

class ProjectController {
  async getAll({ auth, }) {
    const user = await auth.getUser()
    return await ProjectRepository.findAllByUserId(user.id)
  }

  async getById({ auth, params }) {
    const user = await auth.getUser()
    const { id } = params
    return await ProjectRepository.findByIdAndUserId({ id, userId: user.id })
  }

  async create({ auth, request, response }) {
    const { name } = request.all()
    const user = await auth.getUser()
    const countProjects = await ProjectRepository.getCountProjectsByUserId(user.id)
    if (user.getLimitProjects() > countProjects) {
      return await ProjectRepository.create({ name, userId: user.id })
    } else {
      return response.status(401).json({
        error: 'Excess project limit'
      })
    }

  }

  async edit({ auth, params, request }) {
    const { name } = request.all()
    const { id } = params
    const user = await auth.getUser()

    return ProjectRepository.edit({
      id,
      name,
      userId: user.id
    })
  }

  async delete({ auth, response, params }) {
    const { id } = params
    const user = await auth.getUser()

    await ProjectRepository.deleteByIdAndUserId({ id, userId: user.id })

    response.send({
      status: true
    })
  }
}

module.exports = ProjectController
