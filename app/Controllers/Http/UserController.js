'use strict'

const UserRepository = use('App/Repositories/UserRepository')

class UserController {
  async create({ request }) {
    const { username, email, password } = request.post()
    await UserRepository.create({
      username,
      email,
      password
    })

    return {
      status: true
    }
  }

  async get({ auth }) {
    return await auth.getUser();
  }

}

module.exports = UserController
