'use strict'

class Login {
  get validateAll() {
    return true
  }
  get rules() {
    return {
      email: 'required|email|exists:users,email|exists-password-by-email',
      password: 'required|string|min:8'
    }
  }

  get messages() {
    return {
      'email.required': 'You must provide a email address.',
      'email.exists': 'Your email address is not in the database.',
      'email.exists-password-by-email': 'You are registered with the social network.',
      'password.required': 'You must provide a password.',
      'password.string': 'The password must be a string.',
      'password.min': 'The minimum password is 8 characters.'
    }
  }
}

module.exports = Login
