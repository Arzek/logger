'use strict'

class Project {
  get rules() {
    return {
      name: 'required|string',
    }
  }

  get messages() {
    return {
      'name.required': 'You must provide a password name project.',
      'name.string': 'The name project must be a string.',
    }
  }
}

module.exports = Project
