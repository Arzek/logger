'use strict'

class Log {
  get rules() {
    return {
      token: 'required|exists:projects,token',
      env: 'required|string',
      level: 'required|string',
      message: 'required|string',
      context: 'object'
    }
  }
}

module.exports = Log
