'use strict'

class User {
  get validateAll() {
    return true
  }
  get rules() {
    const { request } = this.ctx
    const { password } = request.all()
    return {
      username: 'required|string',
      email: 'required|email|unique:users,email',
      password: 'required|string|min:8',
      passwordRepeat: `required|string|min:8|equals:${password}`
    }
  }

  get messages() {
    return {
      'username.string': 'You must provide a username.',
      'username.string': 'The username must be a string.',

      'email.required': 'You must provide a email address.',
      'email.unique': 'This email is already registered.',

      'password.required': 'You must provide a password.',
      'password.string': 'The password must be a string.',
      'password.min': 'The minimum password is 8 characters.',

      'passwordRepeat.required': 'You must provide a password.',
      'passwordRepeat.string': 'The password must be a string.',
      'passwordRepeat.min': 'The minimum password is 8 characters.',

      'passwordRepeat.equals': 'Passwords specified must be identical.'

    }
  }
}

module.exports = User
