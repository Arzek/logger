'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Log extends Model {
  static get Serializer() {
    return use('App/Models/Serializers/JsonSerializer')
  }
  static castDates(field, value) {
    if (field === 'created_at') {
      return value.format('DD-MM-YYYY HH:mm:ss')
    }
    return super.formatDates(field, value)
  }

  static get hidden() {
    return ['project_id', 'user_id', 'updated_at']
  }
}

module.exports = Log
