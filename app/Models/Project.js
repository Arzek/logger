'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Project extends Model {
  getUser() {
    return this.hasOne('App/Models/User')
  }

  logs() {
    return this.hasMany('App/Models/Log')
  }

  static get Serializer() {
    return use('App/Models/Serializers/JsonSerializer')
  }
}

module.exports = Project
