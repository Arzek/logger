'use strict'

const Env = use('Env')

const io = require('socket.io-client')(Env.get('WS_URl'))
const Promise = require("bluebird")
io.emitAsync = Promise.promisify(io.emit)

class WS {
  static emit(name, data) {
    return io.emitAsync(name, data)
  }
}

module.exports = WS