'use strict'

const Log = use('App/Models/Log')

class LogRepository {
  static async create({ userId, projectId, env, level, message, context }) {
    const log = new Log()
    log.user_id = userId
    log.project_id = projectId
    log.env = env
    log.level = level
    log.message = message
    log.context = context

    await log.save();

    return log
  }

  static getAll({ userId, projectId }) {
    return Log.query()
      .where('user_id', userId)
      .where('project_id', projectId)
      .orderBy('id', 'desc')
      .fetch()
  }

  static getOne({ id, userId, projectId }) {
    return Log.query()
      .where('id', id)
      .where('user_id', userId)
      .where('project_id', projectId)
      .first()
  }

  static async getCountLogs({ userId, projectId }) {
    return Log.query()
      .where('user_id', userId)
      .where('project_id', projectId)
      .getCount()
  }

  static find(id) {
    return Log.find(id)
  }

  static async deleteLast({ userId, projectId }) {
    const log = await Log.query()
      .where('user_id', userId)
      .where('project_id', projectId)
      .orderBy('id', 'asc')
      .first()

    return log.delete()
  }

  static async deleteOneByIds({ id, userId, projectId }) {
    const log = await this.getOne({ id, userId, projectId })
    return log.delete()
  }

  static async deleteById(id) {
    const log = await this.find(id)
    return log.delete()
  }
}

module.exports = LogRepository