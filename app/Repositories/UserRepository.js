'use strict'
const Env = use('Env')
const User = use('App/Models/User')
const Database = use('Database');

class UserRepository {
  static async create(data) {
    const {
      username,
      email,
      password = null,
      name = null,
      avatar = null,
      provider = null,
      providerId = null,
    } = data

    const user = new User()
    user.username = username
    user.email = email
    user.password = password

    user.name = name
    user.avatar = avatar
    user.provider = provider
    user.provider_id = providerId

    user.limit_logs = Env.get('DEFAULT_LIMIT_LOGS')
    user.limit_projects = Env.get('DEFAULT_LIMIT_PROJECTS')
    user.limit_requests = Env.get('DEFAULT_LIMIT_REQUESTS')

    await user.save()
    return user
  }

  static find(id) {
    return User.find(id)
  }

  static findByEmail(email) {
    return User.findBy('email', email)
  }

  static findByProvider({ provider, providerId, email }) {
    return User.query()
      .where('provider', provider)
      .where('provider_id', providerId)
      .where('email', email)
      .first()
  }

  static async deleteById(id) {
    const user = await this.find(id)
    return user.delete()
  }

  static async deleteByEmail(email) {
    const user = await this.findByEmail(email)
    return user.delete()
  }


  static async deleteTokensByUserId(id) {
    return Database.table('tokens')
      .where('user_id', id)
      .delete()
  }
}

module.exports = UserRepository