'use strict'

const Encryption = use('Encryption')
const Project = use('App/Models/Project')

class ProjectRepository {
  static async create(data) {
    const { name, userId } = data

    const project = new Project()
    project.name = name
    project.user_id = userId
    project.token = Encryption.encrypt(project.name + project.user_id + (new Date()))
    await project.save()

    return project
  }

  static async getCountProjectsByUserId(userId) {
    return Number(await Project.query()
      .where('user_id', userId)
      .getCount())
  }

  static async edit({ id, userId, name }) {
    const project = await this.findByIdAndUserId({ id, userId })
    project.name = name
    await project.save()
    return project
  }

  static find(id) {
    return Project.find(id)
  }

  static findByToken(token) {
    return Project.findBy('token', token)
  }

  static findByNameAndUserId({ userId, name }) {
    return Project.query()
      .where('user_id', userId)
      .where('name', name)
      .first()
  }

  static findByIdAndUserId({ userId, id }) {
    return Project.query()
      .where('id', id)
      .where('user_id', userId)
      .first()
  }

  static findByUserId(userId) {
    return Project.query()
      .where('user_id', userId)
      .first()
  }

  static async findAllByUserId(userId) {
    const projects = await Project
      .query()
      .withCount('logs')
      .where('user_id', userId)
      .orderBy('id', 'desc')
      .fetch()

    return projects
  }

  static async deleteByIdAndUserId({ userId, id }) {
    const project = await this.findByIdAndUserId({ id, userId })
    if (project) {
      return project.delete()
    }
  }

  static async deleteById(id) {
    const project = await this.find(id)
    if (project) {
      return project.delete()
    }
  }
}

module.exports = ProjectRepository 